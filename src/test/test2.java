package test;

import com.company.Person;
import com.company.Tree2;
import org.junit.Assert;
import org.junit.Test;

public class test2 {
    @Test
    public void delTest() {
        Tree2 tree1 = new Tree2();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(null, tree1.del(p1));
    }

    @Test
    public void getWidthTest() {
        Tree2 tree1 = new Tree2();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(1, tree1.getWidth());
    }

    @Test
    public void getHeightTest() {
        Tree2 tree1 = new Tree2();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(1, tree1.getHeight());
    }

    @Test
    public void nodesTest() {
        Tree2 tree1 = new Tree2();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(0, tree1.nodes());
    }

    @Test
    public void leavesTest() {
        Tree2 tree1 = new Tree2();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(0, tree1.leaves());
    }

    @Test
    public void sizeTest() {
        Tree2 tree1 = new Tree2();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(1, tree1.size());
    }

    @Test
    public void getSizeTest() {
        Tree2 tree1 = new Tree2();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(1, tree1.size());
    }
}
