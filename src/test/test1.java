package test;

import com.company.Node;
import com.company.Person;
import com.company.Tree;
import com.company.Tree2;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class test1 {

    @Test
    public void delTest() {
        Tree tree1 = new Tree();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(p1, tree1.del(p1));
    }

    @Test
    public void getWidthTest() {
        Tree tree1 = new Tree();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(1, tree1.getWidth());
    }

    @Test
    public void getHeightTest() {
        Tree tree1 = new Tree();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(1, tree1.getHeight());
    }

    @Test
    public void nodesTest() {
        Tree tree1 = new Tree();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(0, tree1.nodes());
    }

    @Test
    public void leavesTest() {
        Tree tree1 = new Tree();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(1, tree1.leaves());
    }

    @Test
    public void sizeTest() {
        Tree tree1 = new Tree();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(1, tree1.size());
    }

    @Test
    public void getSizeTest() {
        Tree tree1 = new Tree();
        Person p1 = new Person("hvhvh", "2", 3, "4");
        tree1.add(p1);

        Assert.assertEquals(1, tree1.size());
    }

}
