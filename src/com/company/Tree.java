package com.company;

import java.util.ArrayList;
import java.util.List;

public class Tree implements BsTree {
    Node root;

    public Tree() {
        root = null;
    }

    public Tree(List<Person> persons) {
        persons.forEach(this::add);
    }

    private Node addRecursive(Node current, Person val) {
        if (current == null) {
            return new Node(val);
        }

        if (val.compare(val, root.value) < 0) {
            current.left = addRecursive(current.left, val);
        } else if (val.compare(val, root.value) > 0) {
            current.right = addRecursive(current.right, val);
        } else {

            return current;
        }

        return current;
    }

    public void add(Person value) {
        root = addRecursive(root, value);
    }


    @Override
    public Person del(Person val) {
        root = deleteRecursive(root, val);
        return val;
    }

    @Override
    public int getWidth() {

        return getMaxWidth(this.root);
    }

     private int getMaxWidth(Node node) {
        int maxWidth = 0;
        int width;
        int h = height(node);
        int i;

        for (i = 1; i <= h; i++) {
            width = getWidth(node, i);
            if (width > maxWidth)
                maxWidth = width;
        }

        return maxWidth;
    }

    private int height(Node node) {
        if (node == null)
            return 0;
        else {

            int lHeight = height(node.left);
            int rHeight = height(node.right);

            return (lHeight > rHeight) ? (lHeight + 1) : (rHeight + 1);
        }
    }

    private int getWidth(Node node, int level) {
        if (node == null)
            return 0;

        if (level == 1)
            return 1;
        else if (level > 1)
            return getWidth(node.left, level - 1)
                    + getWidth(node.right, level - 1);
        return 0;
    }

    @Override
    public int getHeight() {

        return maxDepth(this.root);
    }

    private int maxDepth(Node node) {
        if (node == null)
            return 0;
        else {

            int lDepth = maxDepth(node.left);
            int rDepth = maxDepth(node.right);

            if (lDepth > rDepth)
                return (lDepth + 1);
            else
                return (rDepth + 1);
        }
    }

    @Override
    public int nodes() {
        return getNodeCount(root) - getLeafCount(root);
    }

    int getNodeCount(Node node) {
        if (node == null)
            return 0;

        return 1 + getNodeCount(node.left) + getNodeCount(node.right);
    }

    @Override
    public int leaves() {
        return getLeafCount(root);
    }

    int getLeafCount(Node node) {
        if (node == null)
            return 0;
        if (node.left == null && node.right == null)
            return 1;
        else
            return getLeafCount(node.left) + getLeafCount(node.right);
    }

    private Node deleteRecursive(Node current, Person value) {
        if (current == null) {
            return null;
        }

        if (value == current.value) {

            if (current.left == null && current.right == null) {
                return null;
            }
            if (current.right == null) {
                return current.left;
            }

            if (current.left == null) {
                return current.right;
            }

            Person smallestValue = findSmallestValue(current.right);
            current.value = smallestValue;
            current.right = deleteRecursive(current.right, smallestValue);
            return current;

        }
        if (value.compare(value, root.value) < 0) {
            current.left = deleteRecursive(current.left, value);
            return current;
        }
        current.right = deleteRecursive(current.right, value);
        return current;
    }

    private Person findSmallestValue(Node root) {
        return root.left == null ? root.value : findSmallestValue(root.left);
    }


    @Override
    public void clear() {
        root = null;
    }

    public List<Node> getAllElementsFromTree() {
        List<Node> list = new ArrayList<>();
        list.add(this.root);


        return list;
    }

    @Override
    public int size() {
        return getSize(root);

    }

    private int getSize(Node root) {
        if (root == null) {
            return 0;
        }
        return 1 + getSize(root.left) + getSize(root.right);
    }

    @Override
    public Person[] toArray() {
        Person[] person = new Person[size()];

        return extractValues(this.root).toArray(person);
    }

    private static List<Person> extractValues(Node n) {
        List<Person> result = new ArrayList<>();
        if (n.left != null) {
            result.addAll(extractValues(n.left));
        }

        if (n.right != null) {
            result.addAll(extractValues(n.right));
        }

        result.add(n.value);

        return result;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Tree{");
        sb.append("root=").append(root);
        sb.append('}');
        return sb.toString();
    }
}
