package com.company;

public class Node {

    Person value;
    Node left;
    Node right;

    Node(Person value) {
        this.value = value;
        right = null;
        left = null;
    }

    public Node() {
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer("Node{");
        sb.append("value=").append(value);
        sb.append(", left=").append(left);
        sb.append(", right=").append(right);
        sb.append('}');
        return sb.toString();
    }
}
